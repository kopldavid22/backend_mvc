const express = require('express');
const { check } = require('express-validator');

const usersControllers = require('../controllers/users-controllers');
const fileUpload = require('../middleware/file-upload');

const router = express.Router();
//if we reach this route "/" we will execute our function (1,2,3)=>{}
//pid = place id
router.get('/', usersControllers.getUsers);

router.post(
  '/signup',
  fileUpload.single('image'),
  [check('name').not().isEmpty(), check('email').normalizeEmail().isEmail(), check('password').isLength({ min: 6 })],
  usersControllers.signup
);

router.post('/login', usersControllers.login);

module.exports = router;
